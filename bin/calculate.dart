// ignore_for_file: prefer_initializing_formals, non_constant_identifier_names

// ignore: camel_case_types
class calculate {
  late double num1, num2, sum;
//getter
  double getNum1() {
    return num1;
  }

  double getNum2() {
    return num1;
  }

//setter
  void setNum1(double num1) {
    this.num1 = num1;
  }

  void setNum2(double num2) {
    this.num2 = num2;
  }
//

  double addNum(double num1, double num2) {
    sum = num1 + num2;
    return sum;
  }

  double subtractNum(double num1, double num2) {
    sum = num1 - num2;
    return sum;
  }

  double multiplyNum(double num1, double num2) {
    sum = num1 * num2;
    return sum;
  }

  double divideNum(double num1, double num2) {
    sum = num1 / num2;
    return sum;
  }

//toString
  @override
  String toString() {
    return "sum : ${sum.toStringAsFixed(2)}";
  }
}
