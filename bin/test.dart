import 'dart:io';

import 'calculate.dart';

void main() {
  var cal = new calculate();
  print(
      "Select the choice you want \n1. ADD \n2. SUBTRACT\n3. MULTIPLY\n4. DIVIDE\n5. EXIT");
  int num = int.parse(stdin.readLineSync()!);
  switch (num) {
    case 1:
      {
        print("input num1 : ");
        double num1 = double.parse(stdin.readLineSync()!);
        print("input num2 : ");
        double num2 = double.parse(stdin.readLineSync()!);
        cal.addNum(num1, num2);
        print(cal);
        break;
      }
    case 2:
      {
        print("input num1 : ");
        double num1 = double.parse(stdin.readLineSync()!);
        print("input num2 : ");
        double num2 = double.parse(stdin.readLineSync()!);
        cal.subtractNum(num1, num2);
        print(cal);
        break;
      }
    case 3:
      {
        print("input num1 : ");
        double num1 = double.parse(stdin.readLineSync()!);
        print("input num2 : ");
        double num2 = double.parse(stdin.readLineSync()!);
        cal.multiplyNum(num1, num2);
        print(cal);
        break;
      }
    case 4:
      {
        print("input num1 : ");
        double num1 = double.parse(stdin.readLineSync()!);
        print("input num2 : ");
        double num2 = double.parse(stdin.readLineSync()!);
        cal.divideNum(num1, num2);
        print(cal);
        break;
      }
    case 5:
      {
        break;
      }
  }
}
